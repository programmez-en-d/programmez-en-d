<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        ${!meta_charset}
        <title>${text title}#ifndef{title} Untiled document #findef{title}#ifdef{dlangfr} - dlang-fr#fidef{dlangfr}</title>
        #ifdef{author}
        <meta name="author" content="${text author}" />
        #fidef{author}

        ${!styles}
        <link rel="stylesheet" href="style/whata.css" />
        <link rel="stylesheet" href="style/geshi.css" />
        <link rel="stylesheet" href="style/style.css" />
        ${!scripts}
        <script src="js/fixme.js" async="async"></script>
    </head>
    <body>
        #ifdef{title}
        <h1 id="doc-title">${html-inline title}</h1>
        #fidef{title}
        #ifndef{proofreader}
        <div id="avertissement-relecture">
            <p>
                <strong>Attention :</strong> ce chapitre n'a pas encore été relu, Des erreurs (traduction, grammaire, orthographe, …) sont donc fortement susceptibles d'être de la partie.
                Si vous le souhaitez, vous pouvez améliorer la situation en sélectionnant les passages qui seraient à corriger ou modifier et en cliquant sur "Suggérer une correction". Si vous voulez, vous pouvez <a href="#" onclick="activerRelecture();return false;">voir les passages</a> qui ont déjà été signalés.
            </p>
            <noscript>
                <p id="noscript"> Les outils de relecture nécessitent la prise en charge et l'activation de Javascript.</p>
            </noscript>
        </div>
        #findef{proofreader}
        #ifdef{proofreader}
        <div id="apropos-relecture">
            <p>
                Si vous trouvez une erreur ou un passage à reformuler, vous pouvez le signaler en sélectionnant le passage concerné et en cliquant sur "Suggérer une correction". Vous pouvez également <a href="#" onclick="activerRelecture();return false;">voir les passages</a> qui ont déjà été signalés (ces outils nécessitent Javascript pour fonctionner).
            </p>
        </div>
       #fidef{proofreader}
       <nav>
            <span class="arbo">
                <a class="dlang-fr-link" href="http://dlang-fr.org">dlang-fr</a>
                <span class="fleche">→</span>
                <a href="../index.html"> Cours </a>
                <span class="fleche">→</span>
                <a href="index.html"> Programmer en D </a>
                <span class="fleche">→</span>
                <a href="">${html-inline title}</a>
            </span>
            <span class="precsuiv"><suivprec /></span>
        </nav>
        #fidef{title}
        #ifdef{author}
        <p id="doc-author">${html-inline author}</p>
        #fidef{author}
        #ifdef{date}
        <p id="doc-date">${html-inline date}</p>
        #fidef{date}

        <div id="content" class="whata">
        ${!content}
        </div>

        ${!footnotes}
        <div id="fixme-signaler" style="display:none">
            <p><a href="#" onclick="return fixme();">Suggérer une correction</a></p>
            <noscript>
                <p>
                    Cette fonctionalité nécessite Javascript.
                </p>
            </noscript>
        </div>
        <nav>
            <span class="arbo">
                <a class="dlang-fr-link" href="http://dlang-fr.org">dlang-fr</a>
                <span class="fleche">→</span>
                <a href="../index.html"> Cours </a>
                <span class="fleche">→</span>
                <a href="index.html"> Programmer en D </a>
                <span class="fleche">→</span>
                <a href="">${html-inline title}</a>
            </span>
            <span class="precsuiv"><suivprec /></span>
        </nav>
        #ifdef{dlangfr}
        <script type="text/javascript">
            if (document.location.hostname === "dlang-fr.org") {
                var _paq = _paq || [];
                _paq.push(['trackPageView']);
                _paq.push(['enableLinkTracking']);
                (function() {
                var u=(("https:" == document.location.protocol) ? "https" : "http") + "://stats.galif.eu/piwik//";
                _paq.push(['setTrackerUrl', u+'piwik.php']);
                _paq.push(['setSiteId', 1]);
                var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
                g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
                })();
            }
        </script>
        #fidef{dlangfr}
    </body>
</html>
