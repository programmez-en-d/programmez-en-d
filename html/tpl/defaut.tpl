<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        ${!meta_charset}
        <title>${text title}#ifndef{title} Untiled document #findef{title}</title>
        #ifdef{author}
        <meta name="author" content="${text author}" />
        #fidef{author}

        ${!styles}
        <link rel="stylesheet" href="style/whata.css" />
        <link rel="stylesheet" href="style/defaut.css" />
        ${!scripts}
        <script src="js/fixme.js" async="async"></script>
    </head>
    <body>
        #ifdef{title}
        <h1 id="doc-title">${html-inline title}</h1>
        #fidef{title}
        #ifdef{author}
        <p id="doc-author">${html-inline author}</p>
        #fidef{author}
        #ifdef{date}
        <p id="doc-date">${html-inline date}</p>
        #fidef{date}
        #ifdef{dlangfr}
        <nav>
            <a href="http://dlang-fr.org"> Dlang-fr </a>
            <span class="fleche"> → </span>
            <a href="../index.html"> Liste des cours </a>
            <span class="fleche"> → </span>
            <a href=""> Programmer en D </a>
        </nav>
        #fidef{dlangfr}
        <div id="content" class="whata">
        ${!content}
        </div>
        ${!footnotes}
        #ifdef{dlangfr}
        <script type="text/javascript">
            if (document.location.hostname === "dlang-fr.org") {
                var _paq = _paq || [];
                _paq.push(['trackPageView']);
                _paq.push(['enableLinkTracking']);
                (function() {
                var u=(("https:" == document.location.protocol) ? "https" : "http") + "://stats.galif.eu/piwik//";
                _paq.push(['setTrackerUrl', u+'piwik.php']);
                _paq.push(['setSiteId', 1]);
                var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
                g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
                })();
            }
        </script>
        #fidef{dlangfr}
    </body>
</html>