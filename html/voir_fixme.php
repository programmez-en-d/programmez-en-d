<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
      <meta charset="utf-8" />
      <title>Fixme - passages rapportés</title>
      <style>
        h1 {
            color:#C50;
            font-weight:normal;
            text-align:center;
        }
        section h1 {
            color:#06B;
            text-align:left
        }

        .passage td {
            color:#F55
        }

        ul {
            list-style:none
        }

        li {
            background:#F1F1F1;
            margin-bottom:1em;
            box-shadow: 2px 2px 2px silver
        }

        th {
            text-align:left;
            padding-right:1em;
            color:#777;
            font-weight:normal
        }
        .cmt td {
            color:purple
        }
      </style>
   </head>
   <body>
    <h1> //Fixme : Passages rapportés </h1>
<?php
    $dir = opendir('fixme_data');

    while ($d = readdir($dir)) {
        if ($d[0] !== '.' && is_file($f = 'fixme_data/' . $d)) {
            $href = substr(htmlspecialchars(base64_decode($d)), 1);
            echo '<section><h1>Page <a href="', $href , '">', $href, '</a></h1><ul>';
            $f = fopen($f, 'r');
            while ($l = fgets($f)) {
                $o = json_decode($l);
                echo '<li><table>';
                echo '<tr class="passage"><th>Passage</th><td>', htmlspecialchars($o->passage->selectedText), '</td></tr>';
                echo '<tr class="cmt"><th>Commentaire</th><td>', htmlspecialchars($o->cmt), '</td></tr>';
                echo '<tr class="nom"><th>Nom</th><td>', htmlspecialchars($o->nom), $o->courriel ? ' &lt;' . htmlspecialchars($o->courriel) . '&gt;' : '', '</td></tr>';
                echo '<tr class="anon"><th>Anonyme</th><td>', $o->anonyme ? 'oui' : 'non', '</td></tr>';
                echo '</table></li>';
            }
            fclose($f);
            echo '</ul></section>';
        }
    }
?>
   </body>
</html>
