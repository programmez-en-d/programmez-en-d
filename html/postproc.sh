#!/usr/bin/env sh

for i in *.html; do
    if [ "$i" != 'index.html' ]; then
        bn=`basename -s '.html' $i`
        three_lines=`grep -C 1 " $bn$" ../map.txt`
        before=`echo "$three_lines" | head -n 1 | cut -d" " -f 2`
        res=""
        if [ "$before" != "$bn" ]; then
            res=" <a class=\"chap-prec\" href=\"$before.html\">Chapitre précédent</a>"
        fi
        suiv=""
        after=`echo "$three_lines" | tail -n 1 | cut -d" " -f 2`
        if [ "$after" != "$bn" ]; then
            res="$res <a class=\"chap-suiv\" href=\"$after.html\">Chapitre suivant</a>"
        fi
        sed -i $i -e "s#<suivprec />#$res#g"
        if [ -f corrections/$i ]; then
            sed -i corrections/$i -e "s#<suiv />#<a class=\"chap-suiv\" href=\"../$after.html\">Chapitre suivant</a>#g"
        fi
    fi
done;
