/*
 * Copyright (c) Raphaël JAKSE, 2014
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */


(function (that) {
    "use strict";

    if (location.protocol !== 'http:' && location.protocol !== 'https:') {
        return;
    }

    var to = 0;

    function message(s) {
        alert(s);
    }

//     function getContent(range, beg, end, pbeg, pend) {
//         if (pbeg === pend) {
//             var c = range.toString();
//             return [c, c];
//         }
// 
//         var rangeC = range.cloneRange();
//         rangeC.detach();
//         rangeC.setEndAfter(pbeg);
//         var c1 = rangeC.toString();
// 
//         rangeC = range.cloneRange();
//         rangeC.detach();
//         rangeC.setStartBefore(pend);
//         var c2 = rangeC.toString();
// 
//         return [c1, c2];
//     }
// 
//     function isTitle(node) {
//         return node.nodeName.match(/[hH][1-6]/) && true;
//     }
// 
//     function titleJustBefore(node, root) {
//         var count = 0, text = node.textContent;
//         do {
//             do {
//                 if (isTitle(node)) {
//                     return [node.textContent, count];
//                 } else if (isInterresting(node) && node.textContent === text) {
//                     ++count;
//                 }
//             } while (node.previousSibling && (node = node.previousSibling));
// 
//             node = node.parentNode.previousSibling;
// 
//             if (isTitle(node)) {
//                 return [node.textContent, count];
//             }
// 
//             if (node) {
//                 node = node.lastChild;
//             }
//         } while (node && node !== root);
//         return [null, count];
//     }
// 
//     function isInterresting(node) {
//         if (isTitle(node)) {
//             return true;
//         }
// 
//         var nN = node.nodeName.toLowerCase();
// 
//         if (nN === 'p' || nN === 'li' || nN === 'pre') {
//             return true;
//         }
// 
//         return false;
//     }
// 
//     function blockElement(node) {
//         while (!isInterresting(node)) {
//             node = node.parentNode;
//         }
//         return node;
//     }
// 
//     function continueSelection(passage, tag, tags, i, len) {
// //         var text = tag.textContent, occ = passage.beginOccurence, pos = 0;
// //         do {
// //             pos += text.substr(pos).indexOf(passage.pBeginContent);
// //             if (pos === -1) {
// //                 return null;
// //             }
// //             --occ;
// //         } while (occ);
// 
//         var range = document.createRange();
// //         range.setStart(tag, pos);
// 
//         var pbl = passage.pBeginBefore.length;
//         easySetStart(range, tag, pbl);
//         easySetEnd(
// 
//         while (tag.textContent !== passage.pEnd && passage.pEndNodeName.toLowerCase() === tag.nodeName.toLowerCase() && i + 1 < len) {
//             do {
//                 tag = tags[++i];
//             while (!isInterresting(tag) && i + 1 < len);
//         }
// 
//         if (tag.textContent !== passage.pEnd || passage.pEndNodeName.toLowerCase() !== tag.nodeName.toLowerCase()) {
//             return null;
//         }
// 
//         range.setEnd(tag, passage.pEndContent.length);
//         return range;
//     }
// 
//     function correspondingSelectionRange(passage) {
//         var tag, tags = root.getElementsByTagName('*'), i, len;
//         var beginCandidates = [];
//         for (i = 0, len = tags.length; i < len; ++i) {
//             tag = tags[i];
//             if (isInterresting(tag)) {
//                 if (tag.textContent === passage.pBeginContent) {
//                     beginCandidates.push(tag); 
//                 }
//             }
//         }
// 
//         if(!beginCandidates.length) {
//             return null;
//         }
// 
//         if (beginCandidates.length > 1) {
//             var t_before, j, leng;
//             for (j = 0, leng = beginCandidates.length; j < leng; ++j) {
//                 tag = beginCandidates[j];
//                 t_before = titleJustBefore(tag);
//                 if (t_before === passage.afterTitle) {
//                     return continueSelection(passage, tag, tags, i, len);
//                 }
//             }
//         }
// 
//         return continueSelection(passage, beginCandidates[0], tags, i, len);
//     }
// 
//     function countIdenticalpEndBetween(pBegin, pEnd, range) {
//         if (pBegin === pEnd) {
//             return 0;
//         }
// 
//         var count = 0;
//         var text = pEnd.textContent;
//         var rangeC = range.cloneRange();
//         rangeC.detach();
//         rangeC.setStartAfter(pBegin);
//         rangeC.setEndBefore(pEnd);
//         var i, tag, len, tags = rangeC.cloneContents().getElementsByTagName('*');
//         for (i = 0, len = tags.length; i < len; ++i) {
//             tag = tags[i];
//             if(isInterresting(tag) && tag.textContent === text) {
//                 ++count;
//             }
//         }
//         return count;
//     }

    function init () {
        var root = document.getElementById('content');
        document.getElementById('fixme-signaler').style.display = '';

        var fixmeDiv = null, nameInput, emailInput, commentTA, submitBtn, anonymousCheck, fixmePassageQ;

        function lowestLevelTextNode(node) {
            if (!node) {
                return null;
            }

            while(node.childNodes[0]) {
                node = node.childNodes[0];
            }

            if (node.nodeType === document.TEXT_NODE) {
                return node;
            }

            return nextTextNode(node);
        }

        function nextTextNode(node) {
            if (!node || node === root) {
                return null;
            }

            if (node.nextSibling) {
                return lowestLevelTextNode(node.nextSibling);
            }
            return nextTextNode(node.parentNode);
        }

        function marquerPassage(passage, passageID) {
            var node = lowestLevelTextNode(root);

            var l, offset, pos = 0, range = document.createRange();

            var span = document.createElement('span');
            span.className = 'fixme-anchor';
            span.title = 'Quelqu\'un a suggéré une correction sur ce passage.';
            span._passageID = passageID;

            while (node) {
                l = node.textContent.length;
                if (pos + l > passage.pos) {
                    range.setStart(node, passage.pos - pos);
                    break;
                }
                pos += l;
                node = nextTextNode(node);
            }

            if (!node) {
                return null;
            }

            var selectionEnd = passage.pos + passage.selectedText.length;

            while (node && pos + l < selectionEnd) {
                range.setEnd(node, l);
                node = nextTextNode(node);
                range.surroundContents(span.cloneNode(false));
                if (node) {
                    range.setEnd(node, 0);
                    range.setStart(node, 0);
                    pos += l;
                    l    = node.textContent.length;
                }
            }

            if (!node) {
                return null;
            }

            range.setEnd(node, selectionEnd - pos);
            range.surroundContents(span.cloneNode(false));
        }

        that.fixme = function () {
            var sel = window.getSelection();
            if (sel.rangeCount && !sel.isCollapsed) {
                if (sel.rangeCount > 1) {
                    message("Il n'est pas possible de signaler plusieurs passages.");
                    return;
                }

//                 var range   = sel.getRangeAt(0);
//                 var rangeC = range.cloneRange();
//                 rangeC.detach();
//                 var pBegin  = blockElement(sel.anchorNode);
//                 var pEnd    = blockElement(sel.focusNode);
//                 var content = getContent(rangeC, sel.anchorNode, sel.focusNode, pBegin, pEnd);
//                 rangeC.collapse(false);
//                 rangeC.setStartBefore(pBegin);
//                 var passage = {
//                     beginOccurence: rangeC.toString().split(content[0]).length - 1,
//                     pBeginNodeName: pBegin.nodeName,
//                     pBeginContent : content[0],
//                     pEndNodeName  : pEnd.nodeName,
//                     pEndContent   : content[1],
//                     afterTitle    : titleJustBefore(pBegin, root),
//                     skipEnd       : countIdenticalpEndBetween(pBegin, pEnd, range),
//                     pBegin        : pBegin.textContent,
//                     pEnd          : pEnd.textContent,
//                     text          : range.toString()
//                 };
//                 console.log(passage);


                var range = sel.getRangeAt(0);
                var rangeBefore = range.cloneRange();
                rangeBefore.detach();
                rangeBefore.collapse(true);
                rangeBefore.setStartBefore(root);
                var selectedText = range.toString();
                var passage = {
                    selectedText: selectedText,
                    pos: rangeBefore.toString().length
                };

                if (fixmeDiv) {
                    commentTA.value = '';
                    fixmeDiv.style.display = '';
                } else {
                    fixmeDiv = document.createElement('div');
                    fixmeDiv.id = 'fixme-form';
                    fixmeDiv.appendChild(document.createElement('p'));
                    fixmeDiv.lastChild.appendChild(document.createElement('label'));
                    fixmeDiv.lastChild.lastChild.textContent = 'Votre nom (ou pseudo) :';
                    nameInput = document.createElement('input');
                    nameInput.type = 'text';
                    if (window.localStorage && localStorage.fixme_name !== undefined) {
                        nameInput.value = localStorage.fixme_name;
                    }
                    fixmeDiv.lastChild.lastChild.appendChild(nameInput);
                    fixmeDiv.appendChild(document.createElement('p'));
                    fixmeDiv.lastChild.appendChild(document.createElement('label'));
                    fixmeDiv.lastChild.lastChild.textContent = 'Votre courriel :';
                    emailInput = document.createElement('input');
                    emailInput.type = 'text';

                    if (window.localStorage && localStorage.fixme_email !== undefined) {
                        emailInput.value = localStorage.fixme_email;
                    }

                    fixmeDiv.lastChild.lastChild.appendChild(emailInput);
                    fixmeDiv.appendChild(document.createElement('p'));
                    fixmeDiv.lastChild.appendChild(document.createElement('label'));
                    fixmeDiv.appendChild(document.createElement('p'));
                    fixmeDiv.lastChild.appendChild(document.createElement('label'));
                    fixmeDiv.lastChild.lastChild.textContent = ' Je ne souhaite pas apparaître dans les remerciements';
                    anonymousCheck = document.createElement('input');
                    anonymousCheck.type = 'checkbox';
                    if (window.localStorage && localStorage.fixme_anonymous !== undefined) {
                        anonymousCheck.checked = localStorage.fixme_anonymous === 'true';
                    }
                    fixmeDiv.lastChild.lastChild.insertBefore(anonymousCheck, fixmeDiv.lastChild.lastChild.firstChild);
                    fixmeDiv.appendChild(document.createElement('p'));
                    fixmeDiv.lastChild.appendChild(document.createElement('label'));
                    fixmeDiv.lastChild.lastChild.textContent = 'Votre commentaire sur le passage que vous avez sélectionné :';
                    fixmeDiv.lastChild.lastChild.for = 'fixme-ta-comment';
                    commentTA    = document.createElement('textarea');
                    commentTA.id = 'fixme-ta-comment';
                    fixmeDiv.appendChild(document.createElement('p'));
                    fixmeDiv.lastChild.appendChild(commentTA);
                    submitBtn = document.createElement('input');
                    submitBtn.type = 'submit';
                    submitBtn.value = 'Envoyer';
                    var cancelBtn = document.createElement('input');
                    cancelBtn.value = 'Annuler';
                    cancelBtn.type  = 'button';
                    fixmeDiv.appendChild(document.createElement('p'));
                    fixmeDiv.lastChild.id = 'fixme-buttons';
                    fixmeDiv.lastChild.appendChild(submitBtn);
                    fixmeDiv.lastChild.appendChild(cancelBtn);

                    fixmeDiv.appendChild(document.createElement('p'));
                    fixmeDiv.lastChild.id = 'fixme-legal';
                    fixmeDiv.lastChild.textContent = "En suggérant une correction, vous acceptez qu'elle soit placée sous la licence du document, et que si le document change de licence, votre contribution soit placée sous la nouvelle licence. En cas de doute à ce sujet, contactez-nous. Votre courriel nous servira à vous contacter si besoin est à propos de votre contribution ; aucune autre utilisation en sera faite. Votre nom/pseudo sera attaché à l'éventuelle modification entraînée par votre commentaire, sauf si nous ne le voulez pas. On vous tiendra au courant.";
                    fixmeDiv.appendChild(document.createElement('p'));
                    fixmeDiv.lastChild.id = 'fixme-passage';
                    fixmeDiv.lastChild.textContent = 'Passage : ';
                    fixmePassageQ = document.createElement('q');
                    fixmeDiv.lastChild.appendChild(fixmePassageQ);

                    cancelBtn.onclick = function () {
                        fixmeDiv.style.display = 'none';
                    };
                    document.body.appendChild(fixmeDiv);
                }
                fixmePassageQ.textContent = selectedText;

                submitBtn.onclick = function (e) {
                    if (window.localStorage) {
                        localStorage.fixme_name  = nameInput.value;
                        localStorage.fixme_email = emailInput.value;
                        localStorage.fixme_anonymous = anonymousCheck.checked;
                    }
                    var xhr = new XMLHttpRequest();
                    submitBtn.disabled = true;
                    xhr.open('post', 'fixme-ajax.php?u=' + encodeURIComponent(location.pathname), true);
                    xhr.onreadystatechange = function () {
                            if (xhr.readyState === 4) {
                                submitBtn.disabled = false;
                                if (xhr.status === 200) {
                                    try {
                                        if (JSON.parse(xhr.responseText).error) {
                                            alert("désolé, une erreur (code " + JSON.parse(xhr.responseText).error + ") est survenue. Il est impossible d'enregistrer votre commentaire. Nous vous suggérons de nous contacter directement par mail pour nous envoyer votre suggestion.");
                                        }
                                        else {
                                            alert('Votre commentaire a été envoyé. Merci !');
                                            fixmeDiv.style.display = 'none';
                                        }
                                    } catch (e) {
                                        alert("Désolé, la réponse du serveur n'a pas été comprise, impossible de savoir si votre commentaire a été enregistré. Dans le doute, vous pouvez nous contacter directement par mail.");
                                        throw (e);
                                    }
                                } else {
                                    alert("Désolé, impossible de poster votre commentaire. Vous pouvez retenter votre chance, ou nous contacter directement par mail.");
                                }
                            }
                    };
                    xhr.send(JSON.stringify({passage:passage,nom:nameInput.value,courriel:emailInput.value,cmt:commentTA.value, anonyme:anonymousCheck.checked}));
                    e.preventDefault();
                    return false;
                };
            } else {
                message("Veuillez sélectionner un passage à commenter.");
            }
            return false;
        };

        var existantFIXMEs, xhr = new XMLHttpRequest();
        xhr.open('get', 'fixme-ajax.php?u=' + encodeURIComponent(location.pathname) + '&date=' + (new Date()).toString(), true);
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                existantFIXMEs = JSON.parse(xhr.responseText);
                var i, len = existantFIXMEs.length;
                for(i = 0; i < len; ++i) {
                    marquerPassage(existantFIXMEs[i].passage, i);
                }
            }
        };
        xhr.send();
    }

    that.activerRelecture = function() {
        document.body.classList.toggle('proofread');
    };

    if (document.body) {
        init();
    } else {
        document.addEventListener('DOMContentLoaded', init, false);
    }
}(window));