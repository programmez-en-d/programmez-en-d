[set
    title       = "Entrée formatée"
    partAs      = chapitre
    translator  = "Raphaël Jakse"
    proofreader = "Stéphane Goujet"
]

Il est possible d'indiquer le format des données attendues à l'entrée. Le format indique aussi bien les données à lire que les caractères qui doivent être ignorés.

Les indicateurs de formatage en entrée du D sont similaires à ceux du langage C.

L'indicateur de formatage [c " %s"], que nous avons déjà utilisé dans les chapitres précédents, lit une donnée selon le type de la variable. Par exemple, comme le type de la variable suivante est [c double], les caractères à l'entrée seront lus comme une valeur flottante~ :

[code <<<
   double nombre;

   readf(" %s", &nombre);
>>>]

La chaîne de formatage peut contenir trois types d'information~ :

 - le caractère espace~ : indique zéro, un ou plusieurs caractères blancs dans l'entrée et indique que tous ces caractères devraient être lus et ignorés~ ;
 - indicateur de format~ : comme pour les indicateurs de format de sortie, les indicateurs de format d'entrée commencent par le caractère [c %] et déterminent le format de la donnée à lire~ ;
 - n'importe quel autre caractère~ : indique que ces caractères sont attendus dans l'entrée tels quels, et qu'ils doivent être lus et ignorés.

La chaîne de formatage permet de sélectionner des informations spécifiques dans l'entrée et d'ignorer les autres.

Penchons-nous sur un exemple qui utilise les trois types d'information dans la chaîne de formatage. Supposons que l'on s'attende à voir apparaître le numéro d'étudiant et la classe dans l'entrée dans le format suivant~ :

[input <<<
   numero:123 classe:90
>>>]

Supposons également que les étiquettes [c numero:] et [c classe:] doivent être ignorées. La chaîne de formatage suivante sélectionnera les valeurs du numéro et de la classe et ignorera les autres caractères~ :

[code=d <<<
   int numero;
   int classe;
   readf("numero:%s classe:%s", &numero, &classe);
>>>]

Les caractères surlignés dans [c "[mark numero:]%s [mark classe:]%s"] doivent apparaître tels quels dans l'entrée~ ; [c readf()] les lit et les ignore.

Le caractère espace qui apparaît dans la chaîne de formatage fera que tous les caractères blancs qui apparaîtront exactement à cet endroit seront lus et ignorés.

Comme le caractère [c %] a un sens spécial dans les chaînes de formatage, quand ce caractère lui-même doit être lu et ignoré, il doit être écrit deux fois : [c %%].

Dans le [[part:chaines | chapitre sur les chaînes], pour lire une ligne de donnée depuis l'entrée, nous avons utilisé [c chomp(readln())]. On peut également faire ceci en plaçant un caractère [c \n] à la fin de la chaîne de formatage~ :

[code=d <<<
   import std.stdio;

   void main()
   {
       write("Prénom : ");
       string prenom;
       readf(" %s\n", &prenom);  // ← \n à la fin

      write("Nom : ");
      string nom;
      readf(" %s\n", &nom);      // ← \n à la fin

      write("Âge : ");
      int age;
      readf(" %s", &age);

      writefln("%s %s (%s)", prenom, nom, age);
   }
>>>]

Les caractères [c \n] à la fin des chaînes de formatage quand on lit le nom et le prénom font que les caractères de nouvelle ligne sont lus depuis l'entrée et ignorés. Cependant, il peut toujours être nécessaire de supprimer les caractères blancs se trouvant éventuellement en fin de ligne avec [c chomp()].

[ = Caractères d'indicateur de format

- [c d]~ : lit un entier dans le système décimal~ ;
- [c o]~ : lit un entier dans le système octal~ ;
- [c x]~ : lit un entier dans le système hexadécimal~ ;
- [c f]~ : lit un nombre en virgule flottante~ ;
- [c s]~ : lit selon le type de la variable~ ;
- [c c]~ : lit un caractère. Cet indicateur permet aussi de lire des caractères blancs (ceux-ci ne sont pas ignorés).

Par exemple, si l'entrée contient «~ [c 23 23 23]~ », les valeurs seront lues différemment selon l'indicateur de format utilisé~ :

[code=d <<<
   int nombre_d;
   int nombre_o;
   int nombre_x;

   readf(" %d %o %x", &nombre_d, &nombre_o, &nombre_x);

   writeln("Lu avec %d : ", nombre_d);
   writeln("Lu avec %o : ", nombre_o);
   writeln("Lu avec %x : ", nombre_x);
>>>]

 Même si l'entrée contient trois ensembles identiques de caractères «~ 23~ », les valeurs des variables sont différentes~ :

[output <<<
   Lu avec %d : 23
   Lu avec %o : 19
   Lu avec %x : 35
>>>]

[p Note~ : | très brièvement, 23 vaut [m 2\times 8+3=19] dans le système octal et [m 2\times 16 + 3 = 35] dans le système hexadécimal.]

]
[ = Exercice

   Supposons que l'entrée contient la date dans le format [c année.mois.jour]. Écrivez un programme qui affiche le numéro du mois. Par exemple, si l'entrée est [c 2009.09.30], la sortie doit être 9.

   [[part:corrections/entree_formatee | … La solution]]
]
