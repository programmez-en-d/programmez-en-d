[set
    title       = "Boucles [c for]"
    partAs      = chapitre
    translator  = "Raphaël Jakse"
    proofreader = "Stéphane Goujet"
]

La boucle [c for] a le même but que la boucle [c while]. [c for] permet d'écrire les définitions et expressions relatives à l'itération de la boucle sur la même ligne.

Même si [c for] est utilisée beaucoup moins souvent que [c foreach] en pratique, il est important de comprendre la boucle [c for] en premier. Nous verrons [c foreach] dans un chapitre ultérieur.

[ = Sections de la boucle [c while]

   La boucle [c while] évalue la condition et continue d'exécuter la boucle tant que la condition est vraie. Par exemple, une boucle pour afficher les nombres entre 1 et 10 peut vérifier la condition ''inférieur à 11'' :

   [code=d <<<   while (nombre < 11) >>>]

   Itérer la boucle peut être fait en incrémentant un compteur à la fin de la boucle~ :

   [code=d <<<   ++nombre; >>>]

   Pour être compilable en D, [c nombre] doit avoir été défini avant sa première utilisation~ :

   [code=d <<<   int nombre = 1; >>>]

   Finalement, il y a ce qui est effectivement fait dans le corps de la boucle~ :

   [code=d <<<   writeln(nombre); >>>]

   Ces quatre sections peuvent être définies comme dans les commentaires ci-après~ :

   [code=d <<<
      int nombre = 1;         // ← préparation

      while (nombre < 11) {   // ← vérification de la condition
         writeln(nombre);     // ← travail de la boucle
         ++nombre;            // ← itération
      }
   >>>]

   Les sections d'une boucle [c while] sont exécutées dans l'ordre suivant pendant l'itération de la boucle [c while]~ :

   [code <<<
      préparation

      vérification de la condition
      travail de la boucle
      itération

      vérification de la condition
      travail de la boucle
      itération

      ...
   >>>]

   Une instruction [c break] ou une exception levée peut également terminer la boucle.

]
[ = Les sections d'une boucle [c for]

   [c for] regroupe trois de ces sections sur une seule ligne. Elles sont écrites à l'intérieur des parenthèses de la boucle [c for], séparées par des points-virgules. Le corps de la boucle ne contient que ce que la boucle fait~ :

   [code=d <<<
      for (/* préparation */; /* condition */; /* itération */) {
         /* travail de la boucle */
      }
   >>>]

   Le même code est plus cohérent quand il est écrit avec une boucle [c for]~ :

   [code=d <<<
      for (int nombre = 1; nombre < 11; ++nombre) {
         writeln(nombre);
      }
   >>>]

   L'avantage de la boucle [c for] est plus évident quand le code de la boucle a un grand nombre d'instructions. L'expression qui incrémente la variable d'itération est visible sur la ligne [c for] au lieu d'être au mélangée avec les autres instructions de la boucle.

   Les sections de la boucle [c for] sont exécutées dans le même ordre que la boucle [c while].

   Les instructions [c break] et [c continue] fonctionnent exactement de la même façon dans la boucle [c for].

   La seule différence entre les boucles [c while] et [c for] est l'espace de nom de la variable d'itération. Ceci est expliqué ci-dessous.

   Bien que ce soit généralement le cas, la variable d'itération n'a pas à être un entier, et n'est pas forcément modifiée par une opération d'incrémentation. Par exemple, la boucle suivante est utilisée pour afficher les moitiés des valeurs flottantes précédentes~ :

   [code=d <<<
      for (double valeur = 1; valeur > 0.001; valeur /= 2) {
         writeln(valeur);
      }
   >>>]

   [p Note~ : | l'information énoncée au début de cette partie est techniquement incorrecte mais couvre la majorité des cas d'utilisations de la boucle [c for], surtout pour les programmeurs qui ont de l'expérience en C ou en C++. En réalité, la boucle [c for] du D n'a pas trois sections séparées par des points-virgules. Elle a deux sections, dont la première contient la préparation et la condition de la boucle. Sans aller dans les détails de cette syntaxe, voici comment définir deux variables de types différents dans la section de préparation~ :]

   [code=d <<<
      for ({ int i = 0; double d = 0.5; } i < 10; ++i) {
         writeln("i: ", i, ", d: ", d);
         d /= 2;
      }
   >>>]

   Notez que la section de préparation est la partie entre les accolades qui sont entre les parenthèses et qu'il n'y a pas de point-virgule entre la section de préparation et la section de condition.

]

[ = Les sections peuvent être vides

   Les trois sections de la boucle [c for] peuvent être laissées vides~ :

   - Parfois une variable d'itération n'est pas nécessaire, par exemple parce qu'une variable déjà définie est utilisée.
   - Parfois la sortie de la boucle peut se faire au moyen d'une instruction [c break] au lieu d'être basée sur la condition de boucle.
   - Parfois les expressions d'itération dépendent de certaines conditions qui seront vérifiées dans le corps de la boucle.

   Quand toutes les sections sont vides, la boucle [c for] correspond à une boucle infinie~ :

   [code=d <<<
      for ( ; ; ) {
         // ...
      }
   >>>]

   Une telle boucle peut être utilisée pour ne jamais finir ou finir avec une instruction [c break].

]

[ = L'espace de nom de la variable d'itération

   La seule différence entre la boucle [c for] et la boucle [c while] est l'espace de nom de la variable définie pendant la préparation de la boucle~ : la variable n'est accessible qu'à l'intérieur de la boucle, et non à l'extérieur~ :

   [code=d <<<
      for (int i = 0; i < 5; ++i) {
         // ...
      }

      writeln(i);   // ← Erreur de compilation
                    //   i n'est pas accessible ici
   >>>]

   En revanche, quand la variable est définie dans l'espace de nom qui contient la boucle [c while], le nom est accessible même après la boucle~ :

   [code=d <<<
      int i = 0;

      while (i < 5) {
         // ...
         ++i;
      }

      writeln(i);   // ← Ça fonctionne ; i est accessible ici
   >>>]

   Nous avons vu le conseil de définir les noms le plus près possible de leur première utilisation dans le chapitre précédent. De façon similaire, plus l'espace de nom d'une variable est petit, mieux c'est. De ce fait, quand la variable d'itération n'est pas utilisée hors de la boucle, [c for] est mieux que [c while].

]

[ = Exercices

   # Afficher le tableau 9×9 suivant en utilisant deux boucles [c for], l'une à l'intérieur de l'autre~ :

      [pre <<<
         0,0 0,1 0,2 0,3 0,4 0,5 0,6 0,7 0,8
         1,0 1,1 1,2 1,3 1,4 1,5 1,6 1,7 1,8
         2,0 2,1 2,2 2,3 2,4 2,5 2,6 2,7 2,8
         3,0 3,1 3,2 3,3 3,4 3,5 3,6 3,7 3,8
         4,0 4,1 4,2 4,3 4,4 4,5 4,6 4,7 4,8
         5,0 5,1 5,2 5,3 5,4 5,5 5,6 5,7 5,8
         6,0 6,1 6,2 6,3 6,4 6,5 6,6 6,7 6,8
         7,0 7,1 7,2 7,3 7,4 7,5 7,6 7,7 7,8
         8,0 8,1 8,2 8,3 8,4 8,5 8,6 8,7 8,8
      >>>]

   # Utiliser une ou plusieurs boucles [c for] pour afficher le caractère [c *] de façon à produire des formes géométriques~ :

      [pre <<<
         *
         **
         ***
         ****
         *****
         ******
         *******
         ********
         *********
      >>>]

      [pre <<<
         ********
          ********
           ********
            ********
             ********
              ********
               ********
                ********
                 ********
      >>>]

      etc.

   [[part:corrections/for | … Les solutions]]
]
