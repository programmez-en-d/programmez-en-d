[set
    title       = "Variables"
    partAs      = chapitre
    translator  = "Raphaël Jakse"
    proofreader = "Stéphane Goujet"
]

Les concepts concrets qui sont représentés dans un programme sont appelés [* variable]. Une valeur comme la température de l'air ou un objet plus compliqué comme un moteur de voiture peuvent être des variables d'un programme.

Toute variable a un certain type et une certaine valeur. La plupart des variables ont également un nom mais certaines variables sont anonymes.

Pour un exemple de variable, on peut penser à l'idée du nombre d'étudiants dans une école. Comme le nombre d'étudiants est un nombre entier, [c int] est un type convenable et [c nombreEtudiants] serait un nom suffisemment explicite.

Selon les règles de syntaxe du langage D, une variable est introduite par son type suivi de son nom. Introduire une variable dans le programme s'appelle sa [* définition]. Une fois qu'une variable est définie, son nom se met à représenter sa valeur.

[code=d <<<
   import std.stdio;

   void main()
   {
    // La définition de la variable ; cette définition
    // indique que le type de nombreEtudiants est int :
    int nombreEtudiants;

    // Le nom de la variable devient sa valeur :
    writeln("Il y a ", nombreEtudiants, " étudiants.");
   }
>>>]

Voici ce qu'affiche ce programme :

[output Il y a 0 étudiants.]

Comme on le voit dans cet affichage, la valeur de nombreEtudiants est 0. Cela correspond au tableau des types fondamentaux du chapitre précédent : La valeur initiale de [c int] est 0.

Notez que le nom de la variable, "nombreEtudiants", n'est jamais affiché. En d'autres termes, la sortie du programme n'est pas "Il y a nombreEtudiants étudiants.".

Les valeurs des variables sont changés par l'opérateur [c =]. Cet opérateur affecte des nouvelles valeurs aux variables, il est donc nommé l'opérateur d'affectation :

[code=d <<<
   import std.stdio;

   void main()
   {
      int nombreEtudiants;
      writeln("Il y a ", nombreEtudiants, " étudiants.");

      // Affecter la valeur 200 à la variable nombreEtudiants :
      nombreEtudiants = 200;
      writeln("Il y a maintenant ", nombreEtudiants, " étudiants.");
   }
>>>]

[output <<<
   Il y a 0 étudiants.
   Il y a maintenant 200 étudiants.
>>>]

Quand la valeur d'une variable est connue au moment de la définition d'une variable, la variable peut être définie et affectée en même temps. C'est une pratique importante, elle rend impossible d'utiliser une variable avant de lui affecter la valeur voulue :

[code=d <<<
   import std.stdio;

   void main()
   {
      // Définition et affectation en même temps:
      int nombreEtudiants = 100;

      writeln("Il y a ", nombreEtudiants, " étudiants.");
   }
>>>]

[output Il y a 100 étudiants.]

[ = Exercice

   Définir deux variables pour afficher "J'ai échangé 20 Euros à un taux de 2.11". Vous pouvez utiliser [c double] pour la valeur flottante.

   [[part:corrections/variables | … La solution]]
]
