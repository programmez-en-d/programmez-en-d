[set
    title       = "La boucle do-while"
    partAs      = correction
    translator  = "Raphaël Jakse"
    proofreader = "Stéphane Goujet"
]

Ce programme n'est pas directement lié à la boucle [c do-while] puisque tout problème qui peut être résolu avec une boucle [c do-while] peut également être résolu par les autres types de boucle.

Le programme peut deviner le nombre auquel l'utilisateur pense en réduisant l'intervalle candidat des deux côtés selon les réponses de l'utilisateur. Par exemple, si sa première tentative est 50 et que l'utilisateur répond que le nombre secret est plus grand, le programme sait alors que le nombre doit être dans l'intervalle [m ~[51, 100~]]. Si le programme tente alors un autre nombre dans le milieu de cet intervalle, cette fois on saura que le nombre est soit dans l'intervalle [m ~[51, 75~]], soit dans l'intervalle [m ~[76, 100~]].

Quand la taille de l'intervalle est 1, le programme est sûr que le nombre qui est dedans est le nombre que l'utilisateur a choisi.
