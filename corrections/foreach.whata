[set
    title       = "La boucle [c foreach]"
    partAs      = correction
    translator  = "Raphaël Jakse"
    proofreader = "Stéphane Goujet"
]

Pour obtenir un tableau associatif qui marche dans le sens opposé de [c noms], les types des clés et des valeurs doivent être échangés. Le nouveau tableau associatif doit être de type [c int~[string~]].

En itérant sur les clés et les valeurs du tableau associatif original et en utilisant les clés comme les valeurs et les valeurs comme les clés, on peuple le tableau [c valeurs]~ :

[code=d <<<
	import std.stdio;

	void main()
	{
		string[int] noms = [ 1:"un", 7:"sept", 20:"vingt" ];

		int[string] valeurs;

		foreach (cle, valeur; noms) {
		   valeurs[valeur] = cle;
		}

		writeln(valeurs["vingt"]);
	}
>>>]

