[set
    title       = "assert et enforce"
    partAs      = correction
    translator  = "Raphaël Jakse"
]

# [
   Vous remarquerez que le programme termine normalement quand on saisit [c 06:09] et [c 1:2]. Cepandant, vous pourrez remarquer que le temps de départ ne correspond pas à ce qui a été saisi par l'utilisateur~ :

   [output <<<
       1 heure et 2 minutes après 09:06 donne 10:08.
   >>>]

   Comme vous pouvez le voir, même si le moment qui a été saisi est [c 06:09], la sortie contient [c 09:06]. Cette erreur sera attrapée à l'aide d'une assertion dans le problème suivant.
  ]
# [
   L'erreur d'assertion après la saisie de [c 06:09] et [c 15:2] nous amène à la ligne suivante~ :

   [code=d <<<
       string momentVersChaine(in int heure, in int minute)
       {
           assert((heure >= 0) && (heure <= 23));
           // ...
       }
   >>>]

   Pour que cette assertion échoue, cette fonction doit être appelée avec une heure incorrecte.

   Les deux seuls appels à [c momentVersChaine()] dans le programme ne semble pas avoir de problème~ :

   [code=d <<<
        writefln("%s heures et %s minutes après %s donne  %s.",
                 DuréeHeures, duréeMinutes,
                 momentVersChaine(heuresDepart, minutesDepart),
                 momentVersChaine(endHour, endMinute));
   >>>]

   Une enquête un peu plus poussée devrait révéler la vraie cause du bogue~ : les variables [c heure] et [c minute] sont échangées lors de la lecture du moment de départ~ :

   [code=d <<<
        lireMoment("Moment de départ", minutesDepart, heuresDepart);
   >>>]

    That programming error causes the time to be interpreted as 09:06 and incrementing it by duration 15:2 causes an invalid heure value.

    An obvious correction is to pass the heure and minute variables in the right order:

   [code=d <<<
        lireMoment("Start time", heuresDepart, minutesDepart);
   >>>]

   La sortie~ :

   [output <<<
       Moment de départ ? (HH:MM) 06:09
       Durée ? (HH:MM) 15:2
       15 heures et 2 minutes après 06:09 donne 21:11.
    >>>]
  ]
# [
    Il s'agit de la même assertion~ :

    [code=d <<<
        assert((heure >= 0) && (heure <= 23));
    >>>]

    La raison est que [c ajouterMoment()] peut produire des heures qui sont plus grandes que [c 23]. Ajouter un modulo à la fin garantirait une des sorties de la fonction~ :

    [code=d <<<
       void ajouterDurée(in int heuresDepart, in int minutesDepart,
                         in int DuréeHeures, in int duréeMinutes,
                         out int heuresResultat, out int minutesResultat)
       {
           heuresResultat = heuresDepart + DuréeHeures;
           minutesResultat = minutesDepart + duréeMinutes;

           if (minutesResultat > 59) {
               ++heuresResultat;
           }

           heuresResultat %= 24;
       }
    >>>]

    Notez que la fonction a d'autres problèmes. Par exemple, [c minutesResultat] peut être supérieure à [c 59]. La fonction suivante calcule la valeur des minutes correctement et s'assure que la sortie de la fonction vérifie les spécifications~ :

    [code=d <<<
       void ajouterDurée(in int heuresDepart, in int minutesDepart,
                         in int DuréeHeures, in int duréeMinutes,
                         out int heuresResultat, out int minutesResultat)
       {
           heuresResultat = heuresDepart + DuréeHeures;
           minutesResultat = minutesDepart + duréeMinutes;

           heuresResultat += minutesResultat / 60;
           heuresResultat %= 24;
           minutesResultat %= 60;

           assert((heuresResultat >= 0) && (heuresResultat <= 23));
           assert((minutesResultat >= 0) && (minutesResultat <= 59));
       }
    >>>]
  ]
# [
   Bonne chance.
]
