[set
    title       = "Nombres entiers et opérations arithmétiques"
    partAs      = correction
    translator  = "Raphaël Jakse"
    proofreader = "Stéphane Goujet"
]

# [ On peut utiliser l'opérateur [c /] pour la division et l'opérateur [c %] pour le reste~ :
	[code=d <<<
		import std.stdio;

		void main()
		{
		    int premier;
		    write("Veuillez entrer le premier nombre : ");
		    readf(" %s", &premier);

		    int second;
		    write("Veuillez entrer le second nombre : ");
		    readf(" %s", &second);

		    int quotient = premier / second;
		    int reste = premier % second;

		    writeln(premier, " = ",
		            second, " * ", quotient, " + ", reste);
		}
	>>>]
]
# [ On peut déterminer si le reste est 0 ou non avec une instruction [c if]~ :

	[code=d <<<
		import std.stdio;

		void main()
		{
		    int premier;
		    write("Veuillez entrer le premier nombre : ");
		    readf(" %s", &premier);

		    int second;
		    write("Veuillez entrer le second nombre : ");
		    readf(" %s", &second);

		    int quotient = premier / second;
		    int reste = premier % second;

		    // Nous ne pouvons pas appeler writeln tout de suite avant de déterminer si
		    // le reste est 0 ou non. On doit terminer la ligne plus loin
		    // avec writeln.
		    write(premier, " = ", second, " * ", quotient);

		    // Le reste doit être affiché seulement si non nul.
		    if (reste != 0) {
		        write(" + ", reste);
		    }

		    // On peut maintenant terminer la ligne.
		    writeln();
		}
	>>>]
]
# [ Code~ :
	[code=d <<<
		import std.stdio;

		void main()
		{
		    while (true) {
		        write("0: Quitter, 1: Ajouter, 2: Soustraire, 3: Multiplier,",
		              " 4: Diviser - Veuillez entrer l'opération : ");

		        int operation;
		        readf(" %s", &operation);

		        // Vérifions qu'une opération valide a été entrée
		        if ((operation < 0) || (operation > 4)) {
		            writeln("Je ne connais pas cette opération");
		            continue;
		        }

		        if (operation == 0){
		            writeln("Au revoir !");
		            break;
		        }

		        // Si nous sommes ici, nous savons que nous avons affaire à l'une
		        // des 4 opérations. Maintenant, il est temps de demander
		        // deux entiers à l'utilisateur :

		        int premier;
		        int second;

		        write(" Premier nombre : ");
		        readf(" %s", &premier);

		        write("Second nombre : ");
		        readf(" %s", &second);

		        int resultat;

		        if (operation == 1) {
		            resultat = premier + second;

		        } else if (operation == 2) {
		            resultat = premier - second;

		        } else if (operation == 3) {
		            resultat = premier * second;

		        } else if (operation == 4) {
		            resultat = premier / second;

		        }  else {
		            writeln(
		                "erreur ! ",
		                "Cette condition ne devrait jamais avoir eu lieu.");
		            break;
		        }

		        writeln("       Résultat : ", resultat);
		    }
		}
>>>]
]

# [ Code~ :
	[code=d <<<
		import std.stdio;

		void main()
		{
		    int valeur = 1;

		    while (valeur <= 10) {
		        if (valeur != 7) {
		            writeln(valeur);
		        }

		        ++valeur;
		    }
		}
	>>>]
]
