[set
    title       = "Rediriger l'entrée standard et les flux de sortie"
    partAs      = correction
    translator  = "Raphaël Jakse"
    proofreader = "Stéphane Goujet"
]

Rediriger l'entrée standard et la sortie des programmes est souvent utilisé, spécialement dans les consoles des systèmes de type Unix. Certains programmes sont conçus pour fonctionner correctement quand ils sont combinés avec d'autres programmes.

Par exemple, un fichier nommé [c essai.d] peut être cherché dans une arborescence en combinant [c find] avec [c grep]~ :

[code=bash <<< find | grep essai.d >>>]

[c find] affiche le nom de tous les fichiers sur sa sortie. [c grep] reçoit cette sortie dans son entrée et affiche les lignes qui contiennent [c essai.d] sur sa propre sortie.
